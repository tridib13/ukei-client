import React, {ReactElement, ReactNode} from 'react';
import {View, StyleSheet, StyleProp, TextStyle} from 'react-native';
import {Heading2} from '../Typography';

interface Props {
  icon: ReactElement;
  style?: StyleProp<TextStyle>;
  children: ReactNode;
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export default function HeadingWithIcon(props: Props): ReactElement {
  return (
    <View style={styles.container}>
      {props.icon}
      <Heading2 style={[props.style, {marginHorizontal: 10}]}>
        {props.children}
      </Heading2>
    </View>
  );
}
