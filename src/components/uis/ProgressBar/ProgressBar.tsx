import React, {ReactElement} from 'react';
import {StyleProp, ViewStyle, View} from 'react-native';
import AnimatedBar from 'react-native-animated-bar';
import {useTheme} from '../../../providers/ThemeProvider';

interface Props {
  progress: number;
  style?: StyleProp<ViewStyle>;
}

export default function ProgressBar(props: Props): ReactElement {
  const {theme} = useTheme();
  return (
    <View
      style={[
        {flex: 1, justifyContent: 'center', alignItems: 'center'},
        props.style,
      ]}>
      <AnimatedBar
        progress={props.progress}
        height={7}
        barColor={theme.black}
        fillColor={theme.background}
        borderRadius={4}
      />
    </View>
  );
}
