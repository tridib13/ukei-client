import React, {ReactElement} from 'react';

import {
  View,
  StyleSheet,
  TouchableOpacity,
  StyleProp,
  ViewStyle,
  TouchableOpacityProps,
} from 'react-native';

import {useTheme} from '../../../providers/ThemeProvider';

interface Props extends TouchableOpacityProps {
  checked: boolean;
  width: number;
  startBlock?: boolean;
  endBlock?: boolean;
}

const styles = StyleSheet.create({
  block: {
    height: 10,
  },
  rotateBlock: {
    transform: [{rotate: '180deg'}],
  },
  endBlock: {
    borderRadius: 2,
    borderColor: 'black',
    borderBottomLeftRadius: 50,
    borderTopLeftRadius: 50,
  },
});

export default function IndicatorBlock(props: Props): ReactElement {
  const {theme, themeType} = useTheme();

  const color: StyleProp<ViewStyle> = {
    backgroundColor: props.checked ? theme.black : theme.grey,
  };

  return (
    <TouchableOpacity
      {...props}
      style={[
        styles.block,
        (props.startBlock || props.endBlock) && styles.endBlock,
        props.endBlock && styles.rotateBlock,
        color,
        {width: `${props.width}%`},
        props.style,
      ]}
    />
  );
}
