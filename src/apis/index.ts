export const ROOT_URL = 'http://localhost:4040/api';

type RequestTypes = 'GET' | 'POST' | 'PUT' | 'DELETE';

/**
 * Combines the endpoint path with the base url
 * @param path API endpoint url - no need for the first `/`
 * @param callIAM Sends request to IAM if true, otherwise sends request to the App Backend
 */
function buildApiEndpoint(path: string): string {
  const url = new URL(path, ROOT_URL);

  return url.href;
}

/**
 * A helper function to resolve the first response of the api and convert the response to a json object.
 * Also resolves http status codes and more
 * @param res API Response after fetching
 * @throws Custom `ApiException` error of type `ApiError`
 */
function resolveResponse<T>(res: Response): Promise<T> {
  if (res.ok)
    try {
      return res.json();
    } catch (e) {
      console.warn('Unable to parse response JSON');
    }

  throw new Error();
}

const handleError = async (
  message?: string,
  devMessage?: string,
  res?: Response,
): Promise<void> => {
  // TODO Handle message
  console.error('API Exception', message, devMessage);

  if (res) {
    console.error(res.url);

    console.error(res.status, res.statusText);

    try {
      console.error(await res.json());
    } catch (e) {
      console.warn('Unable to parse response JSON');
    }
  }
};

export const request = async <T>(
  path: string,
  method: RequestTypes = 'GET',
  body?: Record<string, unknown>,
  signal?: AbortSignal | undefined,
): Promise<T> => {
  const fetchOption: RequestInit = {
    signal,
    method,
    mode: 'cors',
    headers: new Headers({
      Accept: 'application/json',
      'Content-Type': 'application/json',
    }),
    body: body ? JSON.stringify(body) : undefined,
  };

  try {
    const res: Response = await fetch(buildApiEndpoint(path), fetchOption);

    return resolveResponse<T>(res);
  } catch (err) {
    throw new Error(err);
  }
};
